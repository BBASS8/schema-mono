import uvicorn
from fastapi import FastAPI, HTTPException, requests
from pydantic import BaseModel
from fastapi.middleware.cors import CORSMiddleware


app = FastAPI()

origins = [
    "http://localhost:3000",
    "http://localhost:8000",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

forms = {
    "aa3795": [
        {"name": "name", "type": "string", "mandatory": True},
        {"name": "email", "type": "email", "mandatory": True},
    ],
    "foo": {
        "title": "my_foo_form",
        "type": "object",
        "required": ["task"],
        "properties": {
            "task": {
                "type": "string",
                "title": "task",
            },
            "answer": {
                "type": "boolean",
                "title": "Done?",
            },
        },
    },
}


class FormSubmission(BaseModel):
    name: str
    email: str


class FooFormSubmission(BaseModel):
    task: str
    done: bool


@app.post("/form/foo")
def receive_form_post(id: str, submission: FooFormSubmission):
    if forms.post(id):
        return submission
    else:
        raise HTTPException(status_code=422, detail=f"unprocessable entity {id}")


@app.get("/form/{id}")
def get_form_description(id: str):
    if forms.get(id):
        return forms[id]
    else:
        raise HTTPException(status_code=404, detail="huh?!")


@app.post("/form/{id}", tags=["forms"])
def submit_form(id: str, form_data: FormSubmission):
    print("Submitted form data:")
    print(form_data.dict())
    return {"message": "Form submitted successfully"}
