import React, { useState, useEffect } from "react";

const FormComponent = () => {
  const [formData, setFormData] = useState([]);

  const fetchData = async () => {
    const response = await fetch("http://localhost:8000/form/foo");

    if (response.ok) {
      const data = await response.json();
      console.log(data);
      setFormData(data);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  const handleSubmit = async (event) => {
    console.log(event);
    event.preventDefault();

    // const response = await fetch("http://localhost:8000/form/foo", {
    // method: 'post'
    // headers: {
    //   'Content-Type': 'application/json',
    // },
    // body: JSON.stringify(data),
  };

  return (
    <form onSubmit={handleSubmit}>
      {formData.map((field, index) => (
        <div key={index}>
          <label htmlFor={field.name}>{field.name}</label>
          {field.type === "string" && (
            <input
              type="text"
              id={field.name}
              name={field.name}
              required={field.mandatory}
            />
          )}
          {field.type === "email" && (
            <input
              type="email"
              id={field.name}
              name={field.name}
              required={field.mandatory}
            />
          )}
        </div>
      ))}
      <button type="submit">Submit</button>
    </form>
  );
};

export default FormComponent;
