import React, { useState, useEffect } from "react";
import Form from "react-jsonschema-form";
import validator from "@rjsf/validator-ajv8";
// const schema = {
//   properties: [
//     { name: "title", type: "string", label: "Title", required: true },
//     {
//       name: "done",
//       type: "boolean",
//       label: "Done?",
//       required: false,
//       default: false,
//     },
//   ],
// };

const MyFormComponent = () => {
  const [formData, setFormData] = useState([]);
  const [schema, setSchema] = useState({});

  const fetchData = async () => {
    const url = "http://localhost:8000/form/foo";
    const fetchConfig = {
      method: "get",
    };

    try {
      const response = await fetch(url, fetchConfig);
      if (response.ok) {
        const schema = await response.json();
        console.log("Schema fetched successfully!");
        console.log(schema);
        setSchema(schema);
      } else {
        console.error("Schema fetched failed.");
      }
    } catch (error) {
      console.error("An error occurred during Schema fetching:", error);
    }
    console.log(schema);
    // setFormData({});
  };

  useEffect(() => {
    fetchData();
  }, []);

  const handleSubmit = async ({ formData }) => {
    console.log(formData);
  };

  return (
    <div>
      <h1>My Form</h1>
      <Form
        validator={validator}
        schema={schema}
        formData={formData}
        onChange={({ formData }) => setFormData(formData)}
        onSubmit={handleSubmit}
      >
        <button type="submit">Submit</button>
      </Form>
    </div>
  );
};

export default MyFormComponent;
