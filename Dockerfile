FROM python:3.9-slim

COPY ./routers /app/routers
COPY ./requirements.txt /app

WORKDIR /app

RUN pip3 install -r requirements.txt

EXPOSE 8000

CMD ["uvicorn", "routers.main:app", "--host=0.0.0.0", "--reload"]
